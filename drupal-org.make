; fadev_d7_site make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = "contrib"

projects[customerror][version] = "1.0"
projects[customerror][subdir] = "contrib"

projects[module_filter][version] = "1.7"
projects[module_filter][subdir] = "contrib"

projects[pathauto][version] = 1.2
projects[pathauto][type] = "module"

projects[shib_auth][version] = 4.0
projects[shib_auth][type] = "module"

projects[uobanner][version] = 1.5
projects[uobanner][type] = "module"
projects[uobanner][download][type] = "get"
projects[uobanner][download][url] = "http://it.uoregon.edu/system/files/uobanner_v1.5.zip"
projects[uobanner][download][subdir] = "sites/all/modules"

; +++++ Themes +++++

; zen
projects[zen][type] = "theme"
projects[zen][version] = "5.1"
projects[zen][subdir] = "contrib"
