; fadev_d7_site make file for local development
core = "7.x"
api = "2"

projects[drupal][version] = "7.x"
; include the d.o. profile base
includes[] = "fadev-722_core-fields_entity_entityref_editablefields-patch.make"
; includes[] = "drupal-org.make"

projects[fadev_d7_site][type] = "profile"
; the public git repo method is ideal, however drush seems to cache the result and so in order to get updated during development the file download method pointing to a tag snapshot on bitbucket is the current solution
projects[fadev_d7_site][download][type] = "file"
projects[fadev_d7_site][download][url] = "https://bitbucket.org/_vid/fadev_d7_site/get/7.x-1.0-beta-1g.tar.gz"
